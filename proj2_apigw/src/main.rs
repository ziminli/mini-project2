use tracing_subscriber::filter::{EnvFilter, LevelFilter};use lambda_http::{run, service_fn, Body, Error, Request, RequestExt, Response};
use serde_json;

// finds the mean and median of the given array
fn calculate_stats(numbers: &[f64]) -> (f64, f64, f64) {
    // Calculate the mean
    let mean = numbers.iter().sum::<f64>() / numbers.len() as f64;

    // Calculate the median
    let mut sorted_numbers = numbers.to_vec();
    sorted_numbers.sort_by(|a, b| a.partial_cmp(b).unwrap());

    let median: f64;
    let mid = sorted_numbers.len() / 2;
    if sorted_numbers.len() % 2 == 0 {
        // If the length is even, take the average of the two middle values
        median = (sorted_numbers[mid - 1] + sorted_numbers[mid]) / 2.0;
    } else {
        // If the length is odd, take the middle value
        median = sorted_numbers[mid] as f64;
    }
    
    // Calculate the standard deviation
    let variance = numbers
        .iter()
        .map(|x| (x - mean).powi(2))
        .sum::<f64>()
        / numbers.len() as f64;
    let std = variance.sqrt();

    (mean, median, std)
}

/// This is the main body for the function.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Extract the query string parameter
    let value_str = event
        .query_string_parameters_ref()
        .and_then(|params| params.first("numbers"))
        .unwrap_or("")
        .trim_start();
        
    let numbers: Vec<f64>;
    
    // if no match in the query parameters
    if value_str == "" {
        // search for the request body
        let body_value: serde_json::Value = serde_json::from_str(&String::from_utf8_lossy(event.body())).map_err(|_| "Error parsing 'body' JSON")?;

        // Extract the "numbers" field from the body JSON
        numbers = body_value
            .get("numbers")
            .and_then(|v| v.as_array())
            .map(|arr| arr.iter().filter_map(|v| v.as_f64()).collect::<Vec<_>>())
            .ok_or("Missing or invalid 'numbers' field")?;
    }
    // if the format is [1,2,3]
    else if value_str.starts_with('[') {
        numbers = serde_json::from_str(value_str).unwrap();
    }
    // if the format is 1,2,3
    else {
        numbers = value_str.split(",").map(|x| x.parse::<f64>().unwrap()).collect();
    }
    
    // Data processing
    let (mean, median, std) = calculate_stats(&numbers);
    let message = format!("Mean: {mean}, Median: {median}, standard deviation: {std}\n");

    // It will be serialized to the right response event automatically by the runtime
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(message.into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
