use tracing_subscriber::filter::{EnvFilter, LevelFilter};use lambda_runtime::{run, service_fn, Error, LambdaEvent};

use serde::{Deserialize, Serialize};

/// This is a made-up example. Requests come into the runtime as unicode
/// strings in json format, which can map to any structure that implements `serde::Deserialize`
/// The runtime pays no attention to the contents of the request payload.
#[derive(Deserialize)]
struct Request {
    numbers: Vec<f64>,
}

/// This is a made-up example of what a response structure may look like.
/// There is no restriction on what it can be. The runtime requires responses
/// to be serialized into json. The runtime pays no attention
/// to the contents of the response payload.
#[derive(Serialize)]
struct Response {
    req_id: String,
    mean: f64,
    median: f64,
    standard_deviation: f64,
}

// finds the mean and median of the given array
fn calculate_stats(numbers: &[f64]) -> (f64, f64, f64) {
    // Calculate the mean
    let mean = numbers.iter().sum::<f64>() / numbers.len() as f64;

    // Calculate the median
    let mut sorted_numbers = numbers.to_vec();
    sorted_numbers.sort_by(|a, b| a.partial_cmp(b).unwrap());

    let median: f64;
    let mid = sorted_numbers.len() / 2;
    if sorted_numbers.len() % 2 == 0 {
        // If the length is even, take the average of the two middle values
        median = (sorted_numbers[mid - 1] + sorted_numbers[mid]) / 2.0;
    } else {
        // If the length is odd, take the middle value
        median = sorted_numbers[mid] as f64;
    }
    
    // Calculate the standard deviation
    let variance = numbers
        .iter()
        .map(|x| (x - mean).powi(2))
        .sum::<f64>()
        / numbers.len() as f64;
    let std = variance.sqrt();

    (mean, median, std)
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
/// - https://github.com/aws-samples/serverless-rust-demo/
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    // Extract some useful info from the request
    let numbers = event.payload.numbers;
    if numbers.is_empty() {
        return Err("The input array is empty".into());
    }
    let (mean, median, std) = calculate_stats(&numbers);

    // Prepare the response
    let resp = Response {
        req_id: event.context.request_id,
        mean: mean,
        median: median,
        standard_deviation: std
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
