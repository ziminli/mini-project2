# Mini-Project 2

A serverless data-processing application which calculates the mean, median, and standard deviation of a list of numbers. It is built using Rust, Cargo Lambda, and deployed to AWS Lambda.

_proj2_apigw_: the version that is integrated with the AWS API Gateway. 

_proj2_non_apigw_: the version that is NOT integrated with the AWS API Gateway.

## With AWS API Gateway Integration

### Structure
<img src="assets/apigw_arch.jpg" alt="APIGW_structure" width=500 height=200>

An AWS REST API Gateway is coupled to the serverless application that sits on AWS Lambda.

The API endpoint is at: https://fuzaor33bh.execute-api.us-east-1.amazonaws.com/default/pro2_http

### Usage

The serverless application takes in a list of numbers and it will process and return back the mean, median, and standard deviation of the list of numbers. Since the trigger is a REST API, one needs to send a REST HTTP request. There are two ways to embed the input (Example input: [1,2,3]):

1. **Query String Parameter**

Direct link query: `https://fuzaor33bh.execute-api.us-east-1.amazonaws.com/default/pro2_http?numbers=[1,2,3]`

using curl: `curl https://fuzaor33bh.execute-api.us-east-1.amazonaws.com/default/pro2_http?numbers=1,2,3`

Using Other REST HTTP utilities, such as [POSTMAN](https://www.postman.com/):

Under the "param" section, set the key to be "numbers" and value to be the input vector (e.g., [1,2,3] or 1,2,3).

2. **Request Body**

Using curl: `curl -X GET 'https://fuzaor33bh.execute-api.us-east-1.amazonaws.com/default/pro2_http' -H "content-type: application/json" -d '{ "numbers": [1,2,3] }'`

POSTMAN: 
Under the "Body" section, set the corresponding key and value. For instance, for JSON format:
```
{
    "numbers": [1,2,3]
}
```

## Cargo Lambda

[Cargo Lambda](https://www.cargo-lambda.info/) is a subcommand for the Rust package manager: Cargo. It can be used to easily build, run, and deploy Rust functions for AWS Lambda. 

For both versions, one can use the following commands:

`make watch`: hot compile the code and boot the local development server to emulate AWS Lambda interactions.

`make invoke`: execute and test an example instance locally. 

`make build`: build the binary executable.

`make deploy`: deploy the executable to AWS Lambda. Requires an IAM role with privileges in AWS Lambda 
